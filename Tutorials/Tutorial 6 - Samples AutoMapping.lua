----------------------------------------------------------------------------------------------------
-- Tutorial 6: Samples AutoMapping
----------------------------------------------------------------------------------------------------

--[[ In this tutorial, you will learn how to jointly use the results of the pitch and level
     analysis in order to create and map automatically a set of samples.

     Open Kontakt and double click in the rack area to create an empty instrument.

     In the directory of this script file you will find 15 samples with 5 different
     pitches, each pitch having 3 levels of velocity.

     In this script, the pitch and level of the samples will be detected and then mapped
     in a way so that:

        - key ranges will cover the full range between the lowest and highest
          detected pitch.

        - velocity ranges will be linearly spread over the full 0 - 127 scale.

     Let's get started!

--]]

--[[ First, print the directory of the loaded script to reveal where the samples are located.
     You can do so by using the global variable 'scriptPath', which points to the directory of the
     executed script.

--]]

local folderPath = scriptPath .. filesystem.preferred("/Samples/Tutorial 6")

print ("The samples are located in " .. folderPath)

--[[ We will start by running some batch analysis on this folder in order to retrieve the pitch
     and level (RMS) values of the samples.

     For more details about pitch and level analysis, please refer to Tutorials 4 & 5 .
--]]

local levelData = mir.detectRMSBatch(folderPath)
local pitchData = mir.detectPitchBatch(folderPath)

--[[ To create an instrument with samples of different velocities and pitches,
     we will follow the steps below:

    1. Create a zone for each sample and find all the unique pitches that the folder contains

    2. For each unique pitch, we will sort the different velocities and spread
       them linearly over the full range (0 - 127).

    3. Finally, in order to fill any gaps between zones, we will spread the key ranges of the zones
       accordingly
--]]


-- Check for valid instrument
if not instrument then
    print("The following error message informs you that the Creator Tools are not "..
          "focused on a Kontakt instrument. To solve this, load an instrument in "..
          "Kontakt and select it from the instrument dropdown menu on top.")
end

--[[Before you start creating new groups, you can delete all the existing groups of your instrument
    (in this case only the default group):
--]]

instrument.groups:reset()

---- and then create a new group:

local g = Group()
instrument.groups:add(g)
g.name = "E-Piano II"


--[[ Let's add a new zone per sample, set the root key, set default valid values
     for tune/keyRanges/velocityRanges and create a unique pitch table.
--]]

local uniqueKeyList = {}

for samplePath, pitch in pairs(pitchData) do

    key = math.floor(pitch + 0.5)     -- rounds the pitch value to the closest semitone

    z = Zone()
    g.zones:add(z)
    z.file = samplePath
    z.rootKey = key
    z.keyRange.low = key
    z.keyRange.high = key
    z.tune = 0 --  use ' (key - pitch) ' if you want to adjust the tuning
    z.velocityRange.low = 0
    z.velocityRange.high = 127

    uniqueKeyList[key] = key   -- fills unique Key table


end

--[[  To find and count the number of all zones that have the same rootKey,
      we will use the following helper:
--]]

function findAllZonesWithSameKey(rootKey)
    local zonesWithSameKeyIdx = {}
    for n,z in pairs(g.zones) do
        if z.rootKey == rootKey then
            table.insert(zonesWithSameKeyIdx, n)
        end
    end
    return zonesWithSameKeyIdx
end

---- We will now spread the samples accross the full velocity range,:

for _,uniqueKey in pairs(uniqueKeyList) do

    -- find the indeces of all zones that have the same key,:
    local zonesWithSameKeyIdx = findAllZonesWithSameKey(uniqueKey)

    -- create a table that contains the zones indeces and their associated level values,:
    local levels = {}
    for _, index in pairs(zonesWithSameKeyIdx) do
        table.insert(levels, {zoneIdx = index, val = levelData[g.zones[index].file]})
    end

    -- and sort the table by level, in ascending order:
    table.sort(levels, function(a,b) return a.val < b.val end)

    -- Finally, we can spread linearly the velocity ranges of these zones:
    local velocityRangePerSample = math.floor(127/#levels)

    for n,level in pairs(levels) do

        --  Note that we have to use the zone idx, which is stored in the above table: ' level.zoneIdx '
        g.zones[level.zoneIdx].velocityRange.low  = (n-1)*velocityRangePerSample
        g.zones[level.zoneIdx].velocityRange.high = (n  )*velocityRangePerSample-1

        if n==#levels then
            g.zones[level.zoneIdx].velocityRange.high = 127
        end

    end

end

--[[ At this point, the rootKey and velocity ranges are set for each zone.
     We will now spread the key ranges, in order to fill any gaps between zones.
--]]

-- We can start by sorting the uniqueKeyList in ascending order,
local tUniqueKeys = {}

-- populate the table that holds the keys,
for k in pairs(uniqueKeyList) do table.insert(tUniqueKeys, k) end

-- and sort the unique keys.
table.sort(tUniqueKeys)

-- We now consider the key distances between sequential zones, in order to fill the gaps between them:

local keyRangeLow = tUniqueKeys[1]
for n,key in pairs(tUniqueKeys) do

    if n<#tUniqueKeys then
        keyRangeHigh = math.floor((tUniqueKeys[n+1] + key)/2)
    else
        keyRangeHigh = key
    end

    zonesWithSameKeyIdx = findAllZonesWithSameKey(key)

    for _, index in pairs(zonesWithSameKeyIdx) do
        g.zones[index].keyRange.high = keyRangeHigh
        g.zones[index].keyRange.low  = keyRangeLow
    end

    keyRangeLow = keyRangeHigh + 1

end


print(g.name .. ' created!')

--[[ Done!

     Don't forget to hit Push(↑), in order to apply to Kontakt all the changes that took place on
     the tools' side.

     You can now try with your own samples folders and create more complex instruments!
--]]

print("You can now press Push(↑) in order to apply the changes to Kontakt.")
