----------------------------------------------------------------------------------------------------
-- Tutorial 1: Navigating through an existing instrument
----------------------------------------------------------------------------------------------------

--[[ To begin, please make sure that Creator Tools and Kontakt are launched and that an instrument
     is loaded in Kontakt.
     Set the focus of the Instrument Editor to a specific instrument by selecting it in the
     instrument drop-down menu.
     You can now see the instrument's structure in the form of a nested tree.


     The 'instrument' object is the main entry point for instrument scripting.
     As you can also see in the instrument tree view, it has two fields: 'name' and 'groups'.


     Let's first print the instrument's name:
--]]

-- Check for valid instrument
if not instrument then
    print("The following error message informs you that the Creator Tools are not "..
          "focused on a Kontakt instrument. To solve this, load an instrument in "..
          "Kontakt and select it from the instrument dropdown menu on top.")
end

print("The selected instrument is called: " .. instrument.name)


--[[ The next field of an instrument is 'groups'. You can iterate over the 'groups' vector of an
     instrument with a numeric for-loop using Lua's size operator # and the index based accessor []
     (this is done in the same way we would iterate over a standard Lua table, with the only
     difference that the 'groups' vector index starts with 0, while Lua tables start with 1).
--]]

local groups = instrument.groups
for i = 0, #groups-1 do
    print("Group index " .. i .. " is named " .. groups[i].name ..
        ", it's volume value is " .. groups[i].volume ..
        " and it's tune value is " .. groups[i].tune)
end

--[[ You can also iterate groups with a generic for-loop that uses Lua's 'pairs()' function
     (to create a so-called stateless iterator):
--]]

for i,g in pairs(instrument.groups) do
    print("Group index " .. i .. " is named " .. g.name ..
          ", it's volume value is ".. g.volume ..
          " and it's tune value is " .. g.tune)
end


--[[ Of course, all the above printed fields (name, volume and tune), as well as the rest of the
     group fields are always displayed in the instrument's tree view.

     In a similar way, you can also iterate over the zones of a group or a whole instrument.
--]]

local count = 0

for i,g in pairs(instrument.groups) do

        print("Group index " .. i .. " is named " .. g.name ..
              ", it's volume value is ".. g.volume ..
              " and it's tune value is " .. g.tune)

        for n,z in pairs(g.zones) do

            print("Zone index " .. n ..
                  " has a volume value equal to " .. z.volume ..
                  " and it's key range is: [" .. z.keyRange.low .. ", " .. z.keyRange.high .."]")

            count = count + 1

        end

end

print("Total number of zones: " .. count)


--[[
     Try to print out another field, for example the root key of a specific zone.
--]]
