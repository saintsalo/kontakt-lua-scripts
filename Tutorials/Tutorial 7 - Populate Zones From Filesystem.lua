----------------------------------------------------------------------------------------------------
-- Tutorial 7: Populating Zones From The File System
----------------------------------------------------------------------------------------------------

--[[ 
     While we used the file system functions in previous tutorials, in this tutorial we will go through an explicit example of
     populating a group with zones using files found in the file system. We will scan the given path and sub-directories and
     create zones based on the number of files found in the scan. 
     We will then populate the zones with these sample files.

     Open Kontakt and double click in the rack area to create an empty instrument.
--]]

-- Check for valid instrument.
if not instrument then
  print("The following error message informs you that the Creator Tools are not "..
        "focused on a Kontakt instrument. To solve this, load an instrument in "..
        "Kontakt and select it from the instrument dropdown menu on top.")
end

-- Reset the instrument groups.
instrument.groups:reset()

-- Set the directory path with the samples. Print the directory of the loaded script to reveal where the samples are 
-- located.
local path = scriptPath .. filesystem.preferred("/Samples/Tutorial 7")
print("The samples are located in " .. path)

-- Declare an empty table which we will fill with the samples.
local samples = {}

-- Fill the table with the sample files from the directory, it is also possible to scan all the sub-directories 
-- recursively as we are doing here.
local i = 1
for _,p in filesystem.directoryRecursive(path) do
    -- We only want the sample files to be added to our table and not the directories, we can do this by checking 
    -- if the item is a file or not.
    if filesystem.isRegularFile(p) then
      -- Then we add only audio files to our table.
      if filesystem.extension(p) == '.wav' or filesystem.extension(p) == '.aif' or filesystem.extension(p) == '.aiff' then
        samples[i] = p
        i = i+1
      end
    end
end

-- A counter for defining the root note of each zone we will create.
local root = 0

-- Create zones and place one file in each of the created groups.
for index, file in next, samples do

    -- Initialize the zone variable.
    local z = Zone()

    -- Add a zone for each sample.
    instrument.groups[0].zones:add(z)

    -- Set the zone root key, high range and low range to the same values thus confining the zone to a single note.
    z.rootKey = root
    z.keyRange.low = root
    z.keyRange.high = root

    -- Populate the attached zone with a sample from our table.
    z.file = file

    -- Increment the root key variable.
    root = root + 1
end

