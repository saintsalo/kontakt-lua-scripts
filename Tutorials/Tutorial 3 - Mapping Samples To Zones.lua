----------------------------------------------------------------------------------------------------
-- Tutorial 3: Mapping samples to zones
----------------------------------------------------------------------------------------------------

--[[ In this tutorial, you will learn how to create zones and map samples to them.

     Open Kontakt and double click in the rack area to create an empty instrument.

     In the directory of this script file you will find 4 samples. In this script, the samples
     will be mapped to single keys, starting from C1 so that "Kick 808 1.wav" is mapped to C1,
     "Snare 808 1.wav" to C1#, "Clap 808X.wav" to D1 and "ClosedHH 808.wav" to D1#.

     Let's get started!

--]]

--[[ First, print the directory of the loaded script to reveal where the samples are located.
     You can do so by using the global variable 'scriptPath', which points to the directory of the
     executed script.

--]]

print ("The samples are located in " .. scriptPath .. filesystem.preferred("/Samples/Tutorial 3"))

----Then create the table "samples" which contains all the samples we want to map:

local samples =
{
    "Kick 808 1.wav",
    "Snare 808 1.wav",
    "Clap 808X.wav",
    "ClosedHH 808.wav",
}


-- Check for valid instrument
if not instrument then
    print("The following error message informs you that the Creator Tools are not "..
          "focused on a Kontakt instrument. To solve this, load an instrument in "..
          "Kontakt and select it from the instrument dropdown menu on top.")
end

--[[Before you start creating new groups, you can delete all the existing groups of your instrument,
(in this case only the default group):
--]]

instrument.groups:reset()

---- and then create a new group:

local g = Group()
instrument.groups:add(g)
g.name = "808kit"


--[[ In the created group you can now add a zone for each one of the 4 samples
     and map the samples of the directory to the zones:
--]]


for index, file in pairs(samples) do


    z = Zone()
    g.zones:add(z)

   z.file = scriptPath .. "/Samples/Tutorial 3/" ..file

end


---- Now you can set the correct key ranges of the zones, starting from C1:


local C1 = 36


for index, zone in pairs(g.zones) do

    -- map all zones to one key per zone, starting from C1:
    local key = C1 + index
    zone.rootKey = key
    zone.keyRange.low = key
    zone.keyRange.high = key

    -- add set the velocity range:
    zone.velocityRange.low = 0
    zone.velocityRange.high = 127
end

--[[ Done!

     Don't forget to hit Push(↑), in order to apply to Kontakt all the changes that took place on
     the tools' side.

     You can now try assigning the same samples only to white keys, starting from C1!
--]]

print("You can now press Push(↑) in order to apply the changes to Kontakt.")
