----------------------------------------------------------------------------------------------------
-- Tutorial 5: Detect level of samples and map to zones velocity
----------------------------------------------------------------------------------------------------

--[[ In this tutorial, you will learn how to analyse the level of samples and
     map them linearly to zone velocity.

     Open Kontakt and double click in the rack area to create an empty instrument.

     In the directory of this script file you will find 4 samples with the same pitch
     but with different velocities.

     In this script, the level of the samples will be detected and mapped
     to their respective velocity.

     Let's get started!

--]]

--[[ First, print the directory of the loaded script to reveal where the samples are located.
     You can do so by using the global variable 'scriptPath', which points to the directory of the
     executed script.

--]]

local folderPath = scriptPath .. filesystem.preferred("/Samples/Tutorial 5")

print ("The samples are located in " .. folderPath)

--[[ The level analysis can run on a single sample, different modes are
     available with their associated function call.

     mir.detectPeak(samplePath)     : Loudest sample value (absolute)
     mir.detectRMS(samplePath)      : Root Mean Square over 400ms frames (100ms hopSize)
     mir.detectLoudness(samplePath) : Perceptual filtering and RMS over 400ms frames (100ms hopSize)

     If you would like to change the frameSize and hopsize of the RMS/Loudness
     analysis, you can call the function with the following arguments:

     mir.detectRMS(samplePath, frameSizeSec, hopSizeSec)
     mir.detectLoudness(samplePath, frameSizeSec, hopSizeSec)

     Let's print the different level values of the first sample.
--]]

print("Peak = " ..     mir.detectPeak(folderPath     .. "/E-Piano Key64 001.wav") .. " dB")
print("RMS  = " ..     mir.detectRMS(folderPath      .. "/E-Piano Key64 001.wav") .. " dB")
print("Loudness = " .. mir.detectLoudness(folderPath .. "/E-Piano Key64 001.wav") .. " dB")

--[[ If your folder contains multiple samples, you can also run a batch analysis on
     the full folder in a non-recursive way.

     mir.detectPeakBatch(folderPath)
     mir.detectRMSBatch(folderPath)
     mir.detectLoudnessBatch(folderPath)

     frameSize and hopSize of RMS and Loudness detection can also be adjusted in
     a similar fashion as for the single sample analysis ' (folderPath, frameSizeSec, hopSizeSec) '

     Let's run a batch analysis to get the samples RMS value.
--]]

local levelData = mir.detectRMSBatch(folderPath)

local count = 0
for _ in pairs(levelData) do count = count + 1 end

print('Number Of Samples Analyzed : ' .. count)

--[[ ' levelData ' is a Lua table with samplePath as Key
     and level as Value.

     Let's create a new simple instrument with this data.
--]]

--[[ First, let's copy the data into an array which we
     can sort in ascending order of level value
--]]

local levels = {}
for samplePath, level in pairs(levelData) do
    table.insert(levels, {path = samplePath, value = level});
end
table.sort(levels, function(a,b) return a.value < b.value end)


-- Check for valid instrument
if not instrument then
    print("The following error message informs you that the Creator Tools are not "..
          "focused on a Kontakt instrument. To solve this, load an instrument in "..
          "Kontakt and select it from the instrument dropdown menu on top.")
end

--[[Before you start creating new groups, you can delete all the existing groups of your instrument,
(in this case only the default group):
--]]

instrument.groups:reset()

---- and then create a new group:

local g = Group()
instrument.groups:add(g)
g.name = "E-Piano Multiple Velocities"


--[[ In the created group you can now add a zone for each one of the 4 samples
     and map the samples of the directory to the zones
     We will map the velocity ranges linearly so they cover the full range (0 - 127)
--]]

for n, level in pairs(levels) do

    local z = Zone()
    g.zones:add(z)

    z.file = level.path

    local key = 64

    -- set the Key

    z.rootKey       = key
    z.keyRange.low  = key
    z.keyRange.high = key

    -- and set the velocity range:

    z.velocityRange.low  = (n-1)*math.floor(127/#levels)
    z.velocityRange.high = (n)*math.floor(127/#levels)-1

    if n==#levels then
        z.velocityRange.high = 127
    end

end

print(g.name .. ' created!')

--[[ Done!

     Don't forget to hit Push(↑), in order to apply to Kontakt all the changes that took place on
     the tools' side.

     You can now try to detect the level of your own samples and map them!
--]]

print("You can now press Push(↑) in order to apply the changes to Kontakt.")
