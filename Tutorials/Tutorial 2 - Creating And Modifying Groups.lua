----------------------------------------------------------------------------------------------------
-- Tutorial 2: Creating and modifying groups
----------------------------------------------------------------------------------------------------

--[[ In the first tutorial you navigated through an existing instrument via Lua.
     In this tutorial, you will learn how to create and modify the groups of an instrument.


     To begin, please make sure that Creator Tools and Kontakt are launched and that an instrument
     is loaded in Kontakt. Set the focus of the Instrument Editor to a specific instrument by
     selecting it in the instrument drop-down menu. You can now see the instrument's structure in
     the form of a nested tree.

     Whenever you make changes to an instrument (either on Kontakt or the Creator Tools), you can
     sync the states of the two applications. To do so, hit the Push(↑) button to apply changes
     from the tools to Kontakt, or the Pull(↓) button to apply changes from Kontakt to the tools.

     When you select a new instrument, the tools will pull the instrument state automatically.
--]]

-- Check for valid instrument
if not instrument then
    print("The following error message informs you that the Creator Tools are not "..
          "focused on a Kontakt instrument. To solve this, load an instrument in "..
          "Kontakt and select it from the instrument dropdown menu on top.")
end


--[[ You can batch add new groups to the very end of the group list.
     In this example 5 new groups will be created and added at the end of the group list.
--]]

for i = 0, 4 do

    instrument.groups:add(Group())

end

--[[ You can also insert a group in a specific position of the group list, for example in the 4th
     position. Note that if the position is already occupied by another group, the new group will
     not replace the old one, but instead all following groups will increase their index by 1.
--]]

instrument.groups:insert(3, Group())

---- Now assign a new empty group to a variable,:

local newGroup = Group()

---- insert it at the very beginning of the group list:

instrument.groups:insert(0, newGroup)

---- and assign a name to the 'name' field of the 'group':

newGroup.name = "My new group"


--[[ You can batch rename all the above groups. In this example the suffix "__d" will be addded to
     all group names:
--]]

for n,g in pairs(instrument.groups) do
    g.name = g.name.."_d"
    print("Group " .. n .. " is called " .. g.name .. " now")
end

---- Note that in order for the changes to apply in Kontakt, the Push(↑) button must be hit.



--[[ In a similar way to inserting groups, you can also remove groups.
     Here is how you can remove the first group of the instrument:
--]]

instrument.groups:remove(0) --remember that the instrument vector index starts from 0


--[[ You can now create a deep copy (all group fiels are copied) of the first group and insert it in
     the beginning of the group list:
--]]


instrument.groups:insert(0, instrument.groups[0])


---- Print to verify that the first 2 groups of the instrument are identical:

print("Group 0 (" .. instrument.groups[0].name .. ") is indentical to group 1 ("
      .. instrument.groups[1].name .. ")!")

--[[ Try renaming only those groups of an instruments that have a certain string
     in their name.

     Note: Lua's 'string.find' function will help you find out whether a string matches
     a certain pattern: http://www.lua.org/manual/5.3/manual.html#pdf-string.find
--]]

--[[ Done!

     Don't forget to hit Push(↑), in order to apply to Kontakt all the changes that took place on
     the tools' side.

     You can now try with your own samples folders and create more complex instruments!
--]]

print("You can now press Push(↑) in order to apply the changes to Kontakt.")
