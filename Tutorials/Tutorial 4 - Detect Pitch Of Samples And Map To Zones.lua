----------------------------------------------------------------------------------------------------
-- Tutorial 4: Detect pitch of samples and map to zones
----------------------------------------------------------------------------------------------------

--[[ In this tutorial, you will learn how to analyse the pitch of samples and
     create zones and map samples to them.

     Open Kontakt and double click in the rack area to create an empty instrument.

     In the directory of this script file you will find 12 samples. In this script, the pitch of the
     samples will be detected and mapped to single keys.

     Let's get started!

--]]

--[[ First, print the directory of the loaded script to reveal where the samples are located.
     You can do so by using the global variable 'scriptPath', which points to the directory of the
     executed script.

--]]

local folderPath = scriptPath .. filesystem.preferred("/Samples/Tutorial 4")

print ("The samples are located in " .. folderPath)

--[[ The pitch analysis can run on a single sample, you can detect the pitch by
     calling the function ' mir.detectPitch( samplePath ) '
     Let's print the pitch of the first sample.
--]]

print("Analysed pitch = " .. mir.detectPitch(folderPath .. "/E-Piano 001.wav"))

--[[ If your folder contains multiple samples, you can also run a batch analysis on
     the full folder in a non-recursive way.
--]]

local pitchData = mir.detectPitchBatch(folderPath)

local count = 0
for _ in pairs(pitchData) do count = count + 1 end

print('Number Of Samples Analyzed : ' .. count)

--[[ ' pitchData ' is Lua table with the samplePath as Key
     and pitch as Value.

     Let's create a new simple instrument with this data.
--]]


-- Check for valid instrument
if not instrument then
    print("The following error message informs you that the Creator Tools are not "..
          "focused on a Kontakt instrument. To solve this, load an instrument in "..
          "Kontakt and select it from the instrument dropdown menu on top.")
end

--[[Before you start creating new groups, you can delete all the existing groups of your instrument,
(in this case only the default group):
--]]

instrument.groups:reset()

---- and then create a new group:

local g = Group()
instrument.groups:add(g)
g.name = "E-Piano"


--[[ In the created group you can now add a zone for each one of the 12 samples
     and map the samples of the directory to the zones with their detected pitch:
--]]


for samplePath, pitchValue in pairs(pitchData) do

    local z = Zone()
    g.zones:add(z)

    z.file = samplePath

    -- Read the pitch and round it to the closest semitone

    local key = math.floor( pitchValue + 0.5 )

    z.rootKey       = key
    z.keyRange.low  = key
    z.keyRange.high = key

    --[[ You can add a tuning correction by looking
         at the difference between the rounded key and
         the detected value
    --]]

    z.tune = key - pitchValue

    -- add set the velocity range:
    z.velocityRange.low = 0
    z.velocityRange.high = 127

end

print(g.name .. ' created!')

--[[ Done!

     Don't forget to hit Push(↑), in order to apply to Kontakt all the changes that took place on
     the tools' side.

     You can now try to detect the pitch of your own samples and map them!
--]]

print("You can now press Push(↑) in order to apply the changes to Kontakt.")
