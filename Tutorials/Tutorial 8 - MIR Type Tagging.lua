----------------------------------------------------------------------------------------------------
-- Tutorial 8: MIR Type Tagging Scan and Implementation 
----------------------------------------------------------------------------------------------------
--[[
	
	In this tutorial we will explore populating a Kontakt instrument with samples using MIR type tagging.

	Samples can be analyzed to determine if they are instruments or drums and then broken down further into 
	which type of instrument or drum the sample resembles.  
	
	We will also use the MIR pitch tagging functionality to set the root note of instrument type samples.	

	We will also declare a number of functions and use them in the script.
	
--]]

-- Function for nicely printing the table results.
function print_r(arr)

    local str = ""
    i = 1
    for index,value in pairs(arr) do
		indentStr = i.."\t"
        str = str..indentStr..index..": "..value.."\n"
        i = i+1
    end
    print(str)

end


-- Function for getting a table's size.
function tableSize(t)

  local count = 0
  for _ in pairs(t) do count = count + 1 end
  return count

end

-- Round a number to the nearest integer.
function round(n)

	return n % 1 >= 0.5 and math.ceil(n) or math.floor(n)

end

-- Confine a zone so that root key, low key and high key are all the same.
function zoneConfine(zone,value)
		zone.rootKey = value
		zone.keyRange.high = value
		zone.keyRange.low = value
end	


-- Function for converting MIR type tags to strings for printing.
function type_tags(v,mode)

	-- If we are looking for the sample type.
	if mode == 'type' then
		if v == DetectSampleType.INVALID then
			v = 'Invalid'
		elseif v == DetectSampleType.INSTRUMENT then
			v = 'Instrument'
		elseif v == DetectSampleType.DRUM then
			v = 'Drum'
		end

	-- If we are looking for the drum type.
	elseif mode == 'drum' then			
		if v == DetectDrumType.INVALID then
			v = 'Invalid'
		elseif v == DetectDrumType.KICK then
			v = 'Kick'
		elseif v == DetectDrumType.SNARE then
			v = 'Snare'
		elseif v == DetectDrumType.CLOSED_HH then
			v = 'Closed Hat'	
		elseif v == DetectDrumType.OPEN_HH then
			v = 'Open Hat'	
		elseif v == DetectDrumType.TOM then
			v = 'Tom'	
		elseif v == DetectDrumType.CYMBAL then
			v = 'Cymbal'	
		elseif v == DetectDrumType.CLAP then
			v = 'Clap'	
		elseif v == DetectDrumType.SHAKER then
			v = 'Shaker'	
		elseif v == DetectDrumType.PERC_DRUM then
			v = 'Perc Drum'	
		elseif v == DetectDrumType.PERC_OTHER then
			v = 'Perc Other'	
		end

	-- If we are looking for the instrument type.
	elseif mode == 'instrument' then
		if v == DetectInstrumentType.INVALID then
			v = 'Invalid'
		elseif v == DetectInstrumentType.BASS then
			v = 'Bass'
		elseif v == DetectInstrumentType.BOWED_STRING then
			v = 'Bowed String'
		elseif v == DetectInstrumentType.BRASS then
			v = 'Brass'	
		elseif v == DetectInstrumentType.FLUTE then
			v = 'Flute'	
		elseif v == DetectInstrumentType.GUITAR then
			v = 'Guitar'	
		elseif v == DetectInstrumentType.KEYBOARD then
			v = 'Keyboard'	
		elseif v == DetectInstrumentType.MALLET then
			v = 'Mallet'	
		elseif v == DetectInstrumentType.ORGAN then
			v = 'Organ'	
		elseif v == DetectInstrumentType.PLUCKED_STRING then
			v = 'Plucked String'	
		elseif v == DetectInstrumentType.REED then
			v = 'Reed'	
		elseif v == DetectInstrumentType.SYNTH then
			v = 'Synth'	
		elseif v == DetectInstrumentType.VOCAL then
			v = 'Vocal'	
		end
	end	

	-- Return the tag.
	return v

end

-- Function for implementing MIR type tags to strings.
function type_to_tag(var,mode)

	local tagVar = {}
	for k,v in pairs(var) do
		tagVar[k] = type_tags(v,mode)
	end

	-- Return the type.
	return tagVar

end 

-- Set the path to the detection folder.
	mirPathBatch = scriptPath..filesystem.preferred('/Samples/Tutorial 8')

-- Error handling: If there is no Kontakt instrument connected inform the user of the error.
if instrument == nil then
    print("Error: The following error message informs you that the Creator Tools are not "..
          "focused on a Kontakt instrument. To solve this, load an instrument in "..
          "Kontakt and select it from the instrument dropdown menu on top. \n")
else

	-- Good to go.
	print ("Instrument connected \n")   

	print("----------------------------------- MIR - Assign Samples To Zones ----------------------------------")

	-- Reset the instrument groups.
	instrument.groups:reset()
	print('Groups reset')

	-- Name the group.
	instrument.groups[0].name = 'Jammer'

	-- Print instrument information.
	print('Total groups: '..#instrument.groups)
	print('Group 0 name: '..instrument.groups[0].name)	

	-- The script takes a few seconds, let the user know the script is alive.
	print("Working.......")

	-- Scan the samples and tag them as instruments or drums. Change the integer type to a string type for printing.
	mirBatchSampleType = type_to_tag(mir.detectSampleTypeBatch(mirPathBatch),'type')
	
	-- Prepare separate tables for drums and instruments.
	local drumSamples = {}
	local instrumentSamples = {}
	local invalidSamples = {}

	-- Loop through the sample type table .
	for k,v in pairs (mirBatchSampleType) do
		-- When a sample was tagged as a drum, detect the specific drum type and store the sample in the drum table.
		if v == 'Drum' then
			local drumDetect = mir.detectDrumType(k)
			drumSamples[k] = drumDetect
		-- When a sample was tagged as an instrument, detect the specific instrument and store the sample in the instrument table.
		elseif v == 'Instrument' then
			local instrumentDetect = mir.detectInstrumentType(k)
			instrumentSamples[k] = instrumentDetect
		elseif v == 'Invalid' then
			invalidSamples[k] = v
		end
	end

	-- Print information about the detections.

	-- How many total samples analyzed and the sample type information for each sample.
	print(tableSize(mirBatchSampleType)..' Samples Detected')
	print_r(mirBatchSampleType)

	-- How many samples detected as drums and the drum type information for each sample.
	print(tableSize(drumSamples)..' Drum Samples Detected')
	print_r(type_to_tag(drumSamples,'drum'))

	-- How many samples detected as instruments and the instrument type information for each sample.
	print(tableSize(instrumentSamples)..' Instrument Samples Detected')
	print_r(type_to_tag(instrumentSamples,'instrument'))

	-- How many samples detected as instruments and the instrument type information for each sample.
	print(tableSize(invalidSamples)..' Invalid Samples Detected')
	print_r(type_to_tag(invalidSamples,'type'))
	
	print('Creating zones')

	-- For instrument samples place each instruments in an octave range that increments with each additional sample.

	-- Counter for the octave increment.
	local instCount = 0
	
	-- Iterate through the samples and place them in zones.
	for k, v in pairs (instrumentSamples) do

			-- Initialize the zone variable.
			z = Zone()

			-- Add the zone.
			instrument.groups[0].zones:add(z)

			-- Use MIR to analyze the sample and return the root note.
			root = round(mir.detectPitch(k))
			
			-- Set the zone low range.
			z.keyRange.low = 36 + (instCount*12)
			
			-- Set the zone root to the correct relation between the sample root and the low range.
			z.rootKey = root - (root - z.keyRange.low)

			-- Set the zone high range so that the zone covers an octave.
			z.keyRange.high = z.keyRange.low+11
			
			-- Attach the sample to the zone.
			z.file = k	

			-- Let the user know.
			print ('File: '..k..' a '..type_tags(v,'instrument')..' added as an instrument with a key range from '..z.keyRange.low..' to '..z.keyRange.high)

			-- Increment the instrument count.
			instCount = instCount + 1

	end

	-- For drum samples we distinguish between drum types and place each sample in a key range corresponding to the type.
	-- Then we increment each key range with each new sample of the same type so that different types occupy different areas
	-- on the keyboard in series.

	-- Counter for each drum type key range.
	local kickCount = 0
	local tomCount = 5
	local snareCount = 10
	local clapCount = 15
	local hatCount = 20
	local cymbCount = 25

	-- A counter to help facilitate looping.
	local tempCount = 0

	-- Iterate through the samples and place them in zones.
	for k, v in pairs (drumSamples) do
		
		-- Select and increment the correct counter according to the drum type.
		if v == DetectDrumType.KICK then
			tempCount = kickCount
			kickCount = kickCount + 1
		elseif v == DetectDrumType.SNARE then
			tempCount = snareCount
			snareCount = snareCount + 1
		elseif v == DetectDrumType.TOM then
			tempCount = tomCount
			tomCount = tomCount + 1
		elseif v == DetectDrumType.CLAP  then
			tempCount = clapCount
			clapCount = clapCount + 1
		elseif v == DetectDrumType.CLOSED_HH or v == DetectDrumType.OPEN_HH then
			tempCount = hatCount
			hatCount = hatCount + 1
		elseif v == DetectDrumType.CYMBAL then
			tempCount = cymbCount
			cymbCount = cymbCount + 1
		end

		-- Initialize the zone variable.
		z = Zone()

		-- Add the zone.
		instrument.groups[0].zones:add(z)

		-- Set the zone root and range.
		zoneConfine(z,tempCount)

		-- Attach the sample to the zone.
		z.file = k	

		-- Let the user know.
		print ('File: '..k..' a '..type_tags(v,'drum')..' added as a drum on note '..tempCount)

	end

	-- Count the total number of zones.
	local zonecounter = 0
	for _,g in pairs(instrument.groups) do
		for n,z in pairs(g.zones) do
			zonecounter = zonecounter + 1
		end
	end
		print("Total instrument zones: " .. zonecounter)

	-- All ready!
	print('Push to Kontakt')

end


