-- Counts groups and zones

groupcounter = 0
zonecounter = 0
for _,g in pairs(instrument.groups) do
   groupcounter = groupcounter + 1

   for n,z in pairs(g.zones) do
       zonecounter = zonecounter + 1

   end
end
print("Groups:" .. groupcounter)
print("Zones:" .. zonecounter)
