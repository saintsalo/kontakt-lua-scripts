-- Duplicates the first zone of first group and appends it at then end
-- then modifies the key range of the copy

g = instrument.groups[0]
z = g.zones[0]
g.zones:add(z)
z = g.zones[1]

z.keyRange.low = 3
z.keyRange.high = 30
