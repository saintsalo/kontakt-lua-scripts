-- Move zone from one group to another

if #instrument.groups < 2 then
    instrument.groups:resize(2)
end

firstgroup = instrument.groups[0]
secondgroup = instrument.groups[1]
z = firstgroup.zones[0]
secondgroup.zones:add(z)
firstgroup.zones:remove(0)
