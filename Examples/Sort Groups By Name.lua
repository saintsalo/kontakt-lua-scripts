function sortGroups(comp)
    local tmp = {}
    for _,g in pairs(instrument.groups) do
        table.insert(tmp, g)
    end
    table.sort(tmp, comp)
    for n,g in pairs(tmp) do
        instrument.groups[n-1] = g
    end
end

sortGroups(function(l,r) return l.name < r.name end)
